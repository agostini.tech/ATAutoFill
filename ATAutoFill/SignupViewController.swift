//
//  SignupViewController.swift
//  ATAutoFill
//
//  Created by Dejan on 10/02/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func signup() {
        self.dismiss(animated: true, completion: nil)
    }
}
